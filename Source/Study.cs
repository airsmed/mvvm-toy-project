﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    class Study
    {
        public string PatientName { get; set; }
        public string StudyDescription { get; set; }
    }
}
