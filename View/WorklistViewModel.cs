﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Model;

namespace ViewModel
{
    class WorklistViewModel
    {
        public ICommand SendStudyCommand { get; set; }  // command for button
        public ObservableCollection<Study> Studies { get; set; }    // studies loaded on Window
        public Study SelectedStudy { get; set; }    // study selected by user

        public WorklistViewModel()
        { 
            // ctor. When containg windows is created, the ctor called
            SendStudyCommand = new RelayCommand<Study>(SendStudy, CanSendString);
            SelectedStudy = null;
            Studies = new ObservableCollection<Study>
            {
                new Study { PatientName = "changyoung", StudyDescription = "T2 Flair" },
                new Study { PatientName = "doohee", StudyDescription = "T2 tse" },
                new Study { PatientName = "jaeho", StudyDescription = "T1 mperage" }
            };

        }

        public void SendStudy(Study study) 
        { 
            // logic invoked when button is press is written here
        }

        public bool CanSendString(Study study)
        {
            // judge whether the button can be enable or not based on selected-study
            return true;
        }
    }
}
